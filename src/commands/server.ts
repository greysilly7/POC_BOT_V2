import { Command } from 'discord-akairo';
import { Message } from 'discord.js';
import fetch from 'node-fetch';

export default class ServerCommand extends Command {
	public constructor() {
		super('server', {
			aliases: ['server'],
			description: {
				content: 'Gives the status up the servers',
				usage: '',
				examples: []
			},
			args: [
				{
					id: 'server'
				}
			],
			ratelimit: 2,
			category: 'utility'
		});
	}

	public async exec(
		message: Message,
		args: { server: string }
	): Promise<Message> {
		const embed = this.client.util.embed();
		if (args.server === 'poc2') {
			const poc2 = await fetch(
				'https://api.mcsrvstat.us/2/poc2.bloodcoffeegames.com'
			)
				.then(response => response.json())
				.then((json: Stats) => json);
			const onlinePlayers = poc2.players.list.join(', \n');

			embed
				.setAuthor('Here Is The Server Status')
				.addFields(
					{
						name: 'Status: ',
						value: `The server is currently ${
							poc2.online ? 'online' : 'offline'
						}`,
						inline: true
					},
					{
						name: 'Current Players',
						value: poc2.players.online,
						inline: false
					},
					{
						name: 'MOTD: ',
						value: poc2.motd.clean,
						inline: true
					},
					{
						name: 'Online Players: ',
						value: onlinePlayers,
						inline: false
					}
				)
				.setColor('BLUE');
		} else if (args.server === 'poc3') {
			const poc3 = await fetch(
				'https://api.mcsrvstat.us/2/poc3.bloodcoffeegames.com'
			)
				.then(response => response.json())
				.then((json: Stats) => json);
			const onlinePlayers = poc3.players.list.join(', \n');
			embed
				.setAuthor('Here Is The Server Status')
				.addFields(
					{
						name: 'Status: ',
						value: `The server is currently ${
							poc3.online ? 'online' : 'offline'
						}`,
						inline: true
					},
					{
						name: 'Current Players',
						value: poc3.players.online,
						inline: false
					},
					{
						name: 'MOTD: ',
						value: poc3.motd.clean,
						inline: true
					},
					{
						name: 'Online Players: ',
						value: onlinePlayers,
						inline: false
					}
				)
				.setColor('BLUE');
		} else {
			embed
				.setAuthor('PLEASE SPECIFY A SERVER')
				.setDescription('Please run "!server (poc3 or poc2)')
				.setColor('RED');
		}

		return message.util.send(embed);
	}
}

export interface Stats {
	ip: string;
	port: number;
	debug: Debug;
	motd: Motd;
	players: Players;
	version: string;
	online: boolean;
	protocol: number;
	hostname: string;
	mods: Mods;
}
export interface Debug {
	ping: boolean;
	query: boolean;
	srv: boolean;
	querymismatch: boolean;
	ipinsrv: boolean;
	cnameinsrv: boolean;
	animatedmotd: boolean;
	cachetime: number;
	apiversion: number;
}
export interface Motd {
	raw?: string[] | null;
	clean?: string[] | null;
	html?: string[] | null;
}
export interface Players {
	online: number;
	max: number;
	list?: string[] | null;
	uuid: string[];
}
export interface Mods {
	names?: string[] | null;
	raw: Raw;
}
export interface Raw {
	0: string;
	1: string;
	2: string;
	3: string;
	4: string;
	5: string;
	6: string;
	7: string;
	8: string;
	9: string;
	10: string;
	11: string;
	12: string;
	13: string;
	14: string;
	15: string;
	16: string;
	17: string;
	18: string;
	19: string;
	20: string;
	21: string;
	22: string;
	23: string;
	24: string;
	25: string;
	26: string;
	27: string;
	28: string;
	29: string;
	30: string;
	31: string;
	32: string;
	33: string;
	34: string;
	35: string;
	36: string;
	37: string;
	38: string;
	39: string;
	40: string;
	41: string;
	42: string;
	43: string;
	44: string;
	45: string;
	46: string;
	47: string;
	48: string;
	49: string;
	50: string;
	51: string;
	52: string;
	53: string;
	54: string;
	55: string;
	56: string;
	57: string;
	58: string;
	59: string;
	60: string;
	61: string;
	62: string;
	63: string;
	64: string;
	65: string;
	66: string;
	67: string;
	68: string;
	69: string;
	70: string;
	71: string;
	72: string;
	73: string;
	74: string;
	75: string;
	76: string;
	77: string;
	78: string;
	79: string;
	80: string;
	81: string;
	82: string;
	83: string;
	84: string;
	85: string;
	86: string;
	87: string;
	88: string;
	89: string;
	90: string;
	91: string;
	92: string;
	93: string;
	94: string;
	95: string;
	96: string;
	97: string;
	98: string;
	99: string;
	100: string;
	101: string;
	102: string;
	103: string;
	104: string;
	105: string;
	106: string;
	107: string;
	108: string;
	109: string;
	110: string;
	111: string;
	112: string;
	113: string;
	114: string;
	115: string;
	116: string;
	117: string;
	118: string;
	119: string;
	120: string;
	121: string;
	122: string;
	123: string;
	124: string;
	125: string;
	126: string;
	127: string;
	128: string;
	129: string;
	130: string;
	131: string;
	132: string;
	133: string;
	134: string;
	135: string;
	136: string;
	137: string;
	138: string;
	139: string;
	140: string;
	141: string;
	142: string;
	143: string;
	144: string;
	145: string;
	146: string;
	147: string;
	148: string;
	149: string;
	150: string;
	151: string;
	152: string;
	153: string;
	154: string;
	155: string;
	156: string;
	157: string;
	158: string;
	159: string;
}
