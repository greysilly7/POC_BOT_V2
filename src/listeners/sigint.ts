import { Listener } from 'discord-akairo';

export default class SigintListener extends Listener {
	public constructor() {
		super('SIGINT', {
			emitter: 'process',
			event: 'SIGINT'
		});
	}

	public async exec(): Promise<void> {
		console.info('Shutting down...');
		this.client.destroy();
		return process.exit(0);
	}
}
