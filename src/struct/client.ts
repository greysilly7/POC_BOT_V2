import { AkairoClient, CommandHandler, ListenerHandler } from 'discord-akairo';
import { Message } from 'discord.js';
import { join } from 'path';
import { inspect, promisify } from 'util';

declare module 'discord-akairo' {
	interface AkairoClient {
		commandHandler: CommandHandler;
		listnerHandler: ListenerHandler;
		utils: typeof Utils;
	}
}

interface BotOptions {
	owner?: string | string[];
	token?: string;
}

class Utils {
	/**
	 * Turns supplied text into Proper Case.
	 * @param text This is the text to turn into Proper Case
	 * @returns {string} example: Hi there buddy => Hi There Buddy
	 */
	toProperCase = (text: string): string => {
		return text.replace(/\w\S*/g, text => {
			return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
		});
	};
	/**
	 * Cleans text and checks for token if found remove it
	 * @param client This is your bots client
	 * @param text This is a string or Promise that returns a string
	 * @returns {Promise<string>}
	 */
	clean = async (client: BotClient, text: string): Promise<string> => {
		text = await text;

		if (typeof text !== 'string') {
			text = inspect(text, { depth: 1 });
		}

		return text
			.replace(/`/g, '`' + String.fromCharCode(8203))
			.replace(/@/g, '@' + String.fromCharCode(8203))
			.replace(
				client.token,
				'mfa.VkO_2G4Qv3T--NO--lWetW_tjND--TOKEN--QFTm6YGtzq9PH--4U--tG0'
			);
	};
	/**
	 * This returns a random element in a array
	 * @param arr This is an array
	 * @returns {unknown}
	 */
	arrayRandom = (arr: string | unknown[]): unknown => {
		return arr[Math.floor(Math.random() * arr.length)];
	};
	/**
	 * Usage: wait(ms)
	 */
	wait = promisify(setTimeout);
}

export default class BotClient extends AkairoClient {
	public config: BotOptions;

	public constructor(options: BotOptions = {}) {
		super(
			{
				ownerID: options.owner
			},
			{
				fetchAllMembers: false,
				messageCacheMaxSize: 10e3,
				messageCacheLifetime: 3600
			}
		);

		this.config = options;
		this.utils = Utils;
	}

	public listnerHandler: ListenerHandler = new ListenerHandler(this, {
		directory: join(__dirname, '..', 'listeners')
	});

	public commandHandler: CommandHandler = new CommandHandler(this, {
		directory: join(__dirname, '..', 'commands'),
		prefix: '!',
		commandUtil: true,
		commandUtilLifetime: 3e5,
		allowMention: true,
		defaultCooldown: 3000,
		argumentDefaults: {
			prompt: {
				modifyStart: (_: Message, str: string): string =>
					`${str}\nType \`cancel\` to cancel the command.`,
				modifyRetry: (_: Message, str: string): string =>
					`${str}\nType \`cancel\` to cancel the command.`,
				timeout: 'Error: Command timed out',
				ended: 'Error: Too many attemps',
				cancel: 'Command cancelled',
				retries: 3,
				time: 30000
			},
			otherwise: ''
		}
	});

	public async _init(): Promise<void> {
		this.commandHandler.useListenerHandler(this.listnerHandler);
		this.commandHandler.loadAll();
		console.log(`Commands Loaded: ${this.commandHandler.modules.size}`);
		this.listnerHandler.setEmitters({
			commandHandler: this.commandHandler,
			listnerHandler: this.listnerHandler,
			process
		});
		this.listnerHandler.loadAll();
		console.log(`Listeners Loaded: ${this.listnerHandler.modules.size}`);
	}

	public async start(): Promise<string> {
		await this._init();
		return super.login(this.config.token);
	}
}
